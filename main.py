# Erforderliche Funktionen aus Modulen importieren
from functions import m_json
from functions import m_pck
# Pfad zur Setup-JSON-Datei
path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
# Metadaten aus der Setup-Datei abrufen
meta_data = m_json.get_metadata_from_setup(path)
# Sensoren auf Verfügbarkeit überprüfen
m_pck.check_sensors()
# Seriennummern der Temperatursensoren zu den Metadaten hinzufügen
metadata = m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home', meta_data, '/home/pi/calorimetry_home/datasheets')
# Calorimetry-Messdaten abrufen
data = m_pck.get_meas_data_calorimetry(metadata)
# Calorimetry-Messdaten protokollieren
m_pck.logging_calorimetry(data, metadata, '/home/pi/calorimetry_home', path)
# Metadaten-JSON-Datei für zukünftige Referenzen archivieren
m_json.archiv_json('datasheets',path, 'data/newton')